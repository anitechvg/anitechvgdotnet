﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;
using System.Diagnostics;
using System.Data;
using Newtonsoft.Json;
using System.Net;



namespace AniTechVg
{
    public class DataLayer
    {
        private string connectionString = WebConfigurationManager.ConnectionStrings["ConnectionS1"].ConnectionString;        
        private SqlConnection conn;

        public String s1 = "id: ", s2 = "categoria: ";
        public int inserted = -1;

        public DataLayer()
        {
            conn = new SqlConnection(connectionString);
        }

        public void prueba()
        {
            
            try
            {
                SqlDataReader myReader = null;
                SqlCommand myCommand = new SqlCommand("SELECT * from categoria ", conn);                

                conn.Open();
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    s1 += (myReader["id"].ToString());
                    s2 += (myReader["categoria"].ToString());
                }

                conn.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public int insertArt(String titulo, String rutaart, String desc, String tipo, String cat, int autorid)
        {
            try
            {                
                titulo = titulo.Replace("\"", "'");
                rutaart = rutaart.Replace("\"", "'");
                desc = desc.Replace("\"", "'");
                tipo = tipo.Replace("\"", "'");
                cat = cat.Replace("\"", "'");
                String rutaimgpral = (rutaart + "/images/imgpral.jpg").Replace("\"", "'");
                String rutaimginfo = (rutaart + "/images/imginfo.jpg").Replace("\"", "'");

                string sql = "insert into dbo.articulos(titulo, rutaart, descripcion, rutaimgpral, rutaimginfo, tipo, categoria, fechapost, autorid) values (@titulo, @rutaart, @descripcion, @rutaimgpral, @rutaimginfo, (select id from dbo.tipo where tipo = @tipo), (select id from dbo.categoria where categoria = @categoria), getdate(), @autorid);SELECT CAST(scope_identity() AS int);";
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.Add("@titulo", SqlDbType.VarChar);
                        cmd.Parameters["@titulo"].Value = titulo;
                        cmd.Parameters.Add("@rutaart", SqlDbType.VarChar);
                        cmd.Parameters["@rutaart"].Value = rutaart;
                        cmd.Parameters.Add("@descripcion", SqlDbType.VarChar);
                        cmd.Parameters["@descripcion"].Value = desc;
                        cmd.Parameters.Add("@rutaimgpral", SqlDbType.VarChar);
                        cmd.Parameters["@rutaimgpral"].Value = rutaimgpral;
                        cmd.Parameters.Add("@rutaimginfo", SqlDbType.VarChar);
                        cmd.Parameters["@rutaimginfo"].Value = rutaimginfo;
                        cmd.Parameters.Add("@tipo", SqlDbType.VarChar);
                        cmd.Parameters["@tipo"].Value = tipo;
                        cmd.Parameters.Add("@categoria", SqlDbType.VarChar);
                        cmd.Parameters["@categoria"].Value = cat;
                        cmd.Parameters.Add("@autorid", SqlDbType.Int);
                        cmd.Parameters["@autorid"].Value = autorid;                       
                        inserted = (int)cmd.ExecuteScalar();
                    }
                }
                return inserted;
            }
            catch (SqlException ex)
            {
                string msg = "Insert Error:";
                msg += ex.Message;
                return inserted;
            }
        }

        public List<Comment> getComments(int artid)
        {
            var url = "http://localhost:8085/AniTechVgPHP/commentary/getComments.php?id=" + artid;              
           var syncClient = new WebClient();

          var content = syncClient.DownloadString(url);

          var jsonObj = JsonConvert.DeserializeObject<List<Comment>>(content);
         
          return jsonObj;
        }

        public SqlDataReader getArt(int x)
        {
            try
            {
                String query = "";
                if (x == -2)
                {
                    query = "select * from articulos order by id desc";
                }
                else
                {
                    query = "select * from articulos where autorid = " +x +" order by id desc";
                }
                SqlDataReader myReader = null;
                SqlCommand myCommand = new SqlCommand(query, conn);

                conn.Open();
                myReader = myCommand.ExecuteReader();                

                
                return myReader;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        public void deleteArt(int x)
        {
            try
            {
                String  query = "delete from articulos where id = " + x;
                
                SqlDataReader myReader = null;
                SqlCommand myCommand = new SqlCommand(query, conn);

                conn.Open();
                myReader = myCommand.ExecuteReader();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                
            }
        }

    }
}