﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AniTechVg
{
    public class Comment
    {
        public string id { get; set; }
        public string idart { get; set; }
        public string iduser { get; set; }
        public string comentario { get; set; }
        public string fechacoment { get; set; }
    }
}