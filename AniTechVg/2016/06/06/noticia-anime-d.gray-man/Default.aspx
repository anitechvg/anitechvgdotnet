<%@ Page Title="" Language="C#" MasterPageFile="~/AniTechVg.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AniTechVg.WebForm1" %>
 <asp:Content ID="Content" ContentPlaceHolderID="head" runat="server">   
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
<meta name="viewport" content="width=device-width, initial-scale=1.0" /> 
<meta name="keywords" content="anime tecnología videojuegos reseñas" /> 
<meta name="description" content="Página dedicada a la creación de contenido editorial de Anime, Tecnología y Videojuegos" /> 
<meta name="author" content="AniTechVg"/> 
<link rel="stylesheet" type="text/css" href="../../../../css/reset.css"/> 
<link rel="stylesheet" type="text/css" href="../../../../css/style.css"/> 
<link rel="stylesheet" type="text/css" href="../../../../css/bjqs.css"/> 
<link rel="stylesheet" type="text/css" href="../../../../css/demo.css"/> 
<script src="../../../../js/main.js"></script> <!-- Gem jQuery --> 
<script src="../../../../js/jquery-1.12.4.min.js"></script> 
<script src="../../../../js/bjqs-1.3.min.js"></script> 
<script src="../../../../ckeditor/ckeditor.js"></script> 
</asp:Content> 
<asp:Content ID="Content1" ContentPlaceHolderID="navbarID" runat="server">
<div id ="navbar">
           <ul class="topnav">
              <li><a href="#home" class="active"><img id="logo-nav" src="../../../../images/logofinal2.jpg"/></a></li>
             <li><a href="http://localhost:11288/Default.aspx?tipo=3 ">Noticias</a></li>
            <li><a href="http://localhost:11288/Default.aspx?tipo=1 ">Reviews</a></li>
           <li><a href="http://localhost:11288/Default.aspx?tipo=2 ">Previews</a></li>
          <li><a href="http://localhost:11288/Default.aspx?tipo=4 ">Features</a></li>
         <% 
             if(Session["sid"] != null){ 
                 %> 
             <li><a href="http://localhost:8080/AniTechVg/usuario/index.jsp">Bienvenid@<%= Session["usn"]%></a></li> 
             <li><a href="http://localhost:8080/AniTechVg/usuario/logout.jsp">Logout</a></li> 
             <% 
             } 
             else
             { 
                 %> 
                 <li><a href="http://localhost:8080/AniTechVg/index.jsp">Login</a></li>
             <%
             }
             %>  
        <li class="icon"><a href="javascript:void(0);" onclick="myFunction()">&#9776;</a>
       </li>
  </ul>
</div>
 </asp:Content> 
<asp:Content ID="Content2" ContentPlaceHolderID="sliderID" runat="server"> 
  <div id="slider" style="background-image: url(../../../../images/pattern.jpg); width: 100%;">
          <div class = "row">
              <div class = "col-4 col-mm-3">				
              </div>
              <div class = "col-4 col-mm-6 col-m-12">
                  <div id="banner-fade">
                        <!-- start Basic Jquery Slider -->
                        <ul class="bjqs">
                            <li><a href ="http://localhost:11288/2016/06/06/se-disparan-los-rumores-de-nuevos-contenidos-para-mario-kart-8/Default.aspx"><img src="../../../../images/img1.jpg" title="Se disparan los rumores de nuevo contenido para MK8"></a></li>
                            <li><a href ="http://localhost:11288/2016/06/06/gamestop-espera-que-se-anuncien-nuevas-consolas-en-e3-2016/Default.aspx"><img src="../../../../images/img2.jpg" title="Se esperan nuevas consolas para este año"></a></li>
                            <li><a href ="http://localhost:11288/2016/06/06/review-trigun/Default.aspx"><img src="../../../../images/img3.jpg" title="Reseña: Trigun"></a></li>
                        </ul>
                        <!-- end Basic jQuery Slider -->
                    </div>
                    <script class="secret-source">
                        jQuery(document).ready(function ($) {
                            $('#banner-fade').bjqs({
                                animtype: 'slide',
                                height: 720,
                                width: 480,
                                responsive: true,
                                randomstart: true
});
                        });
                   </script>
              </div>
             <div class = "col-4 col-mm-3">				
             </div>
         </div>
     </div>
 </asp:Content> 
<asp:Content ID="Content3" ContentPlaceHolderID="sectionsID" runat="server"> 
   <div id="sections" style="background-image: url(../../../../images/pattern2.png); width: 100%;">
            <div class="row">
                <div class ="col-1-5 hide" ></div>
                <div class = "col-3 col-mm-4 col-m-12">                                     
                    <a href ="http://localhost:11288/Default.aspx?cat=1"><img class="section-img" src="../../../../images/anime.jpg" alt="anime"></a>                       
                </div>
                <div class = "col-3 col-mm-4 col-m-12">                    
                    <a href ="http://localhost:11288/Default.aspx?cat=2"><img class="section-img" src="../../../../images/tecnologia.jpg" alt="tecnologia"></a>                    
                </div>
                <div class = "col-3 col-mm-4 col-m-12">                    
                    <a href ="http://localhost:11288/Default.aspx?cat=3"><img class="section-img" src="../../../../images/videojuegos.jpg" alt="videojuegos"></a>
                </div>
                <div class ="col-1-5 hide" ></div>
            </div>
       </div>
 </asp:Content> 
<asp:Content ID="Content4" ContentPlaceHolderID="maincontentID" runat="server"> 
 <div id="maincontent" style="background-image: url(../../../../images/pattern3.png); width: 100%;">
           <div class="row">
                <div class ="col-1-5 hide" ></div>
  <div class = "col-3 col-mm-4 col-m-12"><img src="images/imgpral.jpg" alt="imgpral" style="width: 100%;" /></div>                                     
  <div class = "col-6 col-mm-8 col-m-12">                                     
 <h2> Noticia anime D.Gray Man    </h2>                                 
<p>El n&uacute;mero de julio de la revista Jump Square (Shueisha) ha anunciado que el anime D.Gray-man Hallow contar&aacute; con seis BD/DVD. La web oficial de la serie ha confirmado que el primero de ellos contar&aacute; con tres episodios y el resto contar&aacute; con dos cap&iacute;tulos. En total, la serie estar&aacute; compuesta por 13 episodios. La p&aacute;gina tambi&eacute;n confirma que Katsura Hoshino ilustrar&aacute; las portadas de los BD/DVD. La serie se estrenar&aacute; en la televisi&oacute;n japonesa el 4 de julio.</p>
     <div class ="col-1-5 hide" ></div>
 </div> 
 </div> 
<div class="row">
<div class="col-1-5 hide">&nbsp;</div>
<div class="col-9 col-m-12 col-mm-12">
<% if(Session["sid"] != null){ %>
<form name="commentForm" method="post" action="http://localhost:8085/AniTechVgPHP/commentary/insertComment.php">
	<label for ="com">Dejanos tu comentario:</label>
	<br>
	<textarea name="com" rows="5" cols="45" maxlength="250" style="width: 100%;" required></textarea>
	<input type="hidden" name = "usid" id="usid" value="<%= Session["usid"]%>"></input>
	<input type="hidden" name = "artid" id="artid" value=" 17"></input>
	<input type="submit"></input>
</form>
<% 
} else {
%>
<p>Debes <a href="http://localhost:8080/AniTechVg/index.jsp">iniciar sesión</a> para poder comentar.</p>
<% 
} 
%>
<div class="col-1-5 hide">&nbsp;</div>
</div>
</div> 
<div class="row">
<div class="col-1-5 hide">&nbsp;</div>
<div class="col-9 col-m-9 col-m">
<% 
	var instance = new AniTechVg.DataLayer();
	var list = instance.getComments(17);
	if(list != null) {
		foreach (AniTechVg.Comment x in list)
		{
			%>
			<p>Fecha del comentario:<%= x.fechacoment%></p>		
			<p>Comentario:</p>				
			<p><%= x.comentario%></p>
			<%
	}
}
%>
</div>
<div class="col-1-5 hide">&nbsp;</div>
</div> 
 </asp:Content> 
<asp:Content ID="Content5" ContentPlaceHolderID="footer" runat="server"> 
       <footer class="footer-distributed">
           <div class="footer-left">
              <img class ="footer-img" src="../../../../images/logofinal2.png"/>
             <p class="footer-links">
                <a href="#">Home</a>
               ·
              <a href="#">Blog</a>     
             ·
            <a href="#">About</a>
           ·
          <a href="#">Faq</a>
         ·
        <a href="#">Contact</a>
   </p>
   <p class="footer-company-name">AniTechVg &copy; 2015</p>
</div>
<div class="footer-center">
   <div>
      <i class="fa fa-map-marker"></i>
      <p><span>Av. Madero 255</span> Morelia, Mexico</p>
  </div>
 <div>
    <i class="fa fa-phone"></i>
   <p>+1 555 123456</p>
</div>
<div>
   <i class="fa fa-envelope"></i>
  <p><a href="mailto:support@anitech.vg">support@anitech.vg</a></p>
</div>
</div>
<div class="footer-right">
   <p class="footer-company-about">
      <img class ="footer-img" src="images/contactanos.jpg" alt="contactanos"/>
  </p>
 <div class="footer-icons">
    <a href="http://www.facebook.com/AniTechVG"><img src="../../../../images/facebook64x64.png"/></a>
   <a href="http://www.twitter.com/AniTechVG"><img src="../../../../images/twitter64x64.png"/></a>
  <a href="http://www.g.in/AniTechVG"><img src="../../../../images/google64x64.png"/></a>
</div>
</div>
</footer>
 </asp:Content> 

