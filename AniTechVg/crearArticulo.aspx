﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AniTechVg.Master" AutoEventWireup="true" CodeBehind="crearArticulo.aspx.cs" Inherits="AniTechVg.articulo.crearArticulo" %>
<asp:Content ID="Content6" ContentPlaceHolderID="maincontentID" runat="server">
    <div id ="maincontent" style="background-image: url(images/pattern3.png); width: 100%;">            
        <div class="row">
            <div class="col-1-5 hide"></div>
            <div class="col-9 col-m-12 col-mm-12">
            <form name ="artForm" method="post" action="articulo/insertart.aspx" onsubmit="validateForm()" enctype="multipart/form-data">
                <label for="nombreart">Nombre del artículo:</label>
                <input type="text" id="nombreart" name="nombreart" required/>
                <input type="hidden" id="nombreartConv" name="nombreartConv"/>
                <br>
                <label for="male">Ingrese la imagen principal</label>
                <input type="file" id="UploadedFile" name="UploadedFile" required/>
                <br>                
                <br>
                <label for ="desc">Categoría:</label>
                <select id="SelectCategoria" runat="server" name="SelectCategoria"/>                              
                <br>
                <label for ="tipo">Tipo:</label>
                <select id="SelectTipo" runat="server" name="SelectTipo"/>             
                <br>
                <br>
                <label for ="desc">Descripción:</label>
                <br>
                <textarea name="desc" rows="5" cols="45" maxlength="250" required></textarea>
                <br>
                <label for ="desc">Contenido:</label>
                <br>
                <textarea name="editor1" id="editor1" rows="10" cols="80" required>  
                </textarea>                              
                <script>
                    CKEDITOR.replace('editor1');
                </script>
                <br>
                <input type="submit" value="Crear Articulo" />
            </form>
               </div> 
            <div class="col-1-5 hide"></div>
            </div>
        </div>
</asp:Content>