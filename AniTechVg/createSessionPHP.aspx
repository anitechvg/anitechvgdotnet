﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="createSessionPHP.aspx.cs" Inherits="AniTechVg.createSessionPHP" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="../js/jquery-1.12.4.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                window.document.forms[0].submit();
            });
        </script>
        <title>JSP Page</title>
    </head>
    <body>
        <form method = "post" action = "http://localhost:8085/AniTechVgPHP/createSessionPHP.php">
            <input type ="hidden" name ="usid" value = "<%=Session["usid"]%>">
            <input type ="hidden" name ="usn" value = "<%=Session["usn"]%>">
            <input type ="hidden" name ="ust" value = "<%=Session["ust"]%>">
            <input type ="hidden" name ="sid" value = "<%=Session["sid"]%>">            
        </form>
    </body>
</html>