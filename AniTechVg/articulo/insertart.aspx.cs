﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Specialized;
using System.Text;

namespace AniTechVg.articulo
{
    public partial class insertart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var nombreart = Request.Form["nombreart"];
                var nombreartConv = Request.Form["nombreartConv"];
                var desc = Request.Form["desc"];
                int idus = 2;
                String tipo = "", cat ="", contenido="", loc="";
                var t = "";
                var c = "";
                try
                {
                    contenido = Request.Form["editor1"];
                    t = Request.Form[2];
                    tipo = t[0] + "";
                    c = Request.Form[3];
                    cat = c[0] + "";
                    idus = Convert.ToInt32(Session["usid"]);
                }
                catch (System.Web.HttpRequestValidationException)
                {
                    Response.Write("HttpRequestValidationException");
                }
                catch (NullReferenceException)
                {
                    Response.Write("NullReferenceException");
                }
                catch (InvalidCastException)
                {
                    Response.Write("InvalidCastException");
                }
                
                DateTime rightNow = DateTime.Now;
                String d, m;
                d = rightNow.Day + "";
                m = rightNow.Month + "";
                if (d.Length == 1)
                {
                    d = "0" + d;
                }
                if (m.Length == 1)
                {
                    m = "0" + m;
                }
                
                if (Request.Files["UploadedFile"] != null)
                {
                    HttpPostedFile MyFile = Request.Files["UploadedFile"];                    
                    //Setting location to upload files

                    loc = rightNow.Year + "/" + m + "/" + d + "/" + nombreartConv;
                    var TargetLocation = Server.MapPath("~/" + rightNow.Year + "/" + m + "/" + d + "/" + nombreartConv + "/images/");
                    Response.Write(loc);
                    if (!Directory.Exists(TargetLocation))
                    {
                        Directory.CreateDirectory(TargetLocation);
                    }
                        try

                        {
                            if (MyFile.ContentLength > 0)
                            {
                                //Determining file name. You can format it as you wish.
                                string FileName = "imgtemp.jpg";
                                
                                //Determining file size.
                                int FileSize = MyFile.ContentLength;
                                
                                //Creating a byte array corresponding to file size.
                                byte[] FileByteArray = new byte[FileSize];
                                
                                //Posted file is being pushed into byte array.
                                MyFile.InputStream.Read(FileByteArray, 0, FileSize);
                                
                                //Uploading properly formatted file to server.
                                MyFile.SaveAs(TargetLocation + FileName);

                                Response.Write( "="+tipo + cat + "=");
                                crearThumbnail(loc, tipo + cat + "");

                            }
                        }
                        catch (Exception BlueScreen)
                        {
                            //Handle errors
                        }
                         var instance = new DataLayer();
                        t = Request.Form[2];
                        tipo = t + "";
                        c = Request.Form[3];
                        cat = c + "";
                            Response.Write(tipo + "__" + cat);
                        
                        saveFile(contenido, loc, nombreart, instance.insertArt(nombreart, loc, desc, cat, tipo, idus));
                        Response.Redirect("http://localhost:11288/");
                }
            }            
        }

        public void crearThumbnail(String TargetLocation, String tipo)
        {
            Image img = Image.FromFile(Server.MapPath("~/" + TargetLocation + "/images/imgtemp.jpg"));
            Graphics g = Graphics.FromImage(img);
            // Place a.gif
            Rectangle rect = new Rectangle(0, 630, 720, 450);

            g.DrawImage(Image.FromFile(Server.MapPath("~/templates/"+tipo+".jpg")), rect);

            img.Save(Server.MapPath("~/" + TargetLocation + "/images/imgpral.jpg"), ImageFormat.Jpeg);

            img = new Bitmap(480, 720);
            g = Graphics.FromImage(img);
            // Place a.gif
            rect = new Rectangle(0, 0, 480, 720);

            g.DrawImage(Image.FromFile(Server.MapPath("~/" + TargetLocation + "/images/imgpral.jpg")), rect);

            img.Save(Server.MapPath("~/" + TargetLocation + "/images/imgpralmini.jpg"), ImageFormat.Jpeg);
        }

        protected void saveFile(String contenido, String loc, String titulo, int inserted)
        {
            using (StreamWriter _testData = new StreamWriter(Server.MapPath("~/"+ loc + "/Default.aspx"), true))
            {
                contenido = "<%@ Page Title=\"\" Language=\"C#\" MasterPageFile=\"~/AniTechVg.Master\" AutoEventWireup=\"true\" CodeBehind=\"Default.aspx.cs\" Inherits=\"AniTechVg.WebForm1\" %>\n" +

   " <asp:Content ID=\"Content\" ContentPlaceHolderID=\"head\" runat=\"server\">   \n " +

        "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /> \n" +
        "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" /> \n" +
        "<meta name=\"keywords\" content=\"anime tecnología videojuegos reseñas\" /> \n" +
        "<meta name=\"description\" content=\"Página dedicada a la creación de contenido editorial de Anime, Tecnología y Videojuegos\" /> \n" +
        "<meta name=\"author\" content=\"AniTechVg\"/> \n" +
        "<link rel=\"stylesheet\" type=\"text/css\" href=\"../../../../css/reset.css\"/> \n" +
        "<link rel=\"stylesheet\" type=\"text/css\" href=\"../../../../css/style.css\"/> \n" +
        "<link rel=\"stylesheet\" type=\"text/css\" href=\"../../../../css/bjqs.css\"/> \n" +
        "<link rel=\"stylesheet\" type=\"text/css\" href=\"../../../../css/demo.css\"/> \n" +
        "<script src=\"../../../../js/main.js\"></script> <!-- Gem jQuery --> \n" +
        "<script src=\"../../../../js/jquery-1.12.4.min.js\"></script> \n" +
        "<script src=\"../../../../js/bjqs-1.3.min.js\"></script> \n" +
        "<script src=\"../../../../ckeditor/ckeditor.js\"></script> \n" +
         "</asp:Content> \n" +
"<asp:Content ID=\"Content1\" ContentPlaceHolderID=\"navbarID\" runat=\"server\">\n" +
"<div id =\"navbar\">\n" +
 "           <ul class=\"topnav\">\n" +
  "              <li><a href=\"#home\" class=\"active\"><img id=\"logo-nav\" src=\"../../../../images/logofinal2.jpg\"/></a></li>\n" +
   "             <li><a href=\"http://localhost:11288/Default.aspx?tipo=3 \">Noticias</a></li>\n" +
    "            <li><a href=\"http://localhost:11288/Default.aspx?tipo=1 \">Reviews</a></li>\n" +
     "           <li><a href=\"http://localhost:11288/Default.aspx?tipo=2 \">Previews</a></li>\n" +
      "          <li><a href=\"http://localhost:11288/Default.aspx?tipo=4 \">Features</a></li>\n" +
       "         <% \n" +
   "             if(Session[\"sid\"] != null){ \n" +
   "                 %> \n" +
   "             <li><a href=\"http://localhost:8080/AniTechVg/usuario/index.jsp\">Bienvenid@<%= Session[\"usn\"]%></a></li> \n" +
   "             <li><a href=\"http://localhost:8080/AniTechVg/usuario/logout.jsp\">Logout</a></li> \n" +
   "             <% \n" +
   "             } \n" +
   "             else\n" +
   "             { \n" +
   "                 %> \n" +
   "                 <li><a href=\"http://localhost:8080/AniTechVg/index.jsp\">Login</a></li>\n" +
   "             <%\n" +
   "             }\n" +
   "             %>  \n" +
        "        <li class=\"icon\"><a href=\"javascript:void(0);\" onclick=\"myFunction()\">&#9776;</a>\n" +
         "       </li>\n" +
          "  </ul>\n" +
    "</div>\n" +
    " </asp:Content> \n" +
    "<asp:Content ID=\"Content2\" ContentPlaceHolderID=\"sliderID\" runat=\"server\"> \n" +
  "  <div id=\"slider\" style=\"background-image: url(../../../../images/pattern.jpg); width: 100%;\">\n" +
  "          <div class = \"row\">\n" +
  "              <div class = \"col-4 col-mm-3\">				\n" +
  "              </div>\n" +
  "              <div class = \"col-4 col-mm-6 col-m-12\">\n" +
  "                  <div id=\"banner-fade\">\n" +

"                        <!-- start Basic Jquery Slider -->\n" +
"                        <ul class=\"bjqs\">\n" +
"                            <li><a href =\"http://localhost:11288/2016/06/06/se-disparan-los-rumores-de-nuevos-contenidos-para-mario-kart-8/Default.aspx\"><img src=\"../../../../images/img1.jpg\" title=\"Se disparan los rumores de nuevo contenido para MK8\"></a></li>\n" +
"                            <li><a href =\"http://localhost:11288/2016/06/06/gamestop-espera-que-se-anuncien-nuevas-consolas-en-e3-2016/Default.aspx\"><img src=\"../../../../images/img2.jpg\" title=\"Se esperan nuevas consolas para este año\"></a></li>\n" +
"                            <li><a href =\"http://localhost:11288/2016/06/06/review-trigun/Default.aspx\"><img src=\"../../../../images/img3.jpg\" title=\"Reseña: Trigun\"></a></li>\n" +
"                        </ul>\n" +
"                        <!-- end Basic jQuery Slider -->\n" +

"                    </div>\n" +


"                    <script class=\"secret-source\">\n" +

"                        jQuery(document).ready(function ($) {\n" +

"                            $('#banner-fade').bjqs({\n" +
"                                animtype: 'slide',\n" +
"                                height: 720,\n" +
"                                width: 480,\n" +
"                                responsive: true,\n" +
"                                randomstart: true\n" +
                            "});\n" +

"                        });\n" +
 "                   </script>\n" +
  "              </div>\n" +
   "             <div class = \"col-4 col-mm-3\">				\n" +
   "             </div>\n" +
   "         </div>\n" +
   "     </div>\n" +
    " </asp:Content> \n" +
     "<asp:Content ID=\"Content3\" ContentPlaceHolderID=\"sectionsID\" runat=\"server\"> \n" +
"   <div id=\"sections\" style=\"background-image: url(../../../../images/pattern2.png); width: 100%;\">\n" +
"            <div class=\"row\">\n" +
"                <div class =\"col-1-5 hide\" ></div>\n" +
"                <div class = \"col-3 col-mm-4 col-m-12\">                                     \n" +
"                    <a href =\"http://localhost:11288/Default.aspx?cat=1\"><img class=\"section-img\" src=\"../../../../images/anime.jpg\" alt=\"anime\"></a>                       \n" +
"                </div>\n" +
"                <div class = \"col-3 col-mm-4 col-m-12\">                    \n" +
"                    <a href =\"http://localhost:11288/Default.aspx?cat=2\"><img class=\"section-img\" src=\"../../../../images/tecnologia.jpg\" alt=\"tecnologia\"></a>                    \n" +
"                </div>\n" +
"                <div class = \"col-3 col-mm-4 col-m-12\">                    \n" +
"                    <a href =\"http://localhost:11288/Default.aspx?cat=3\"><img class=\"section-img\" src=\"../../../../images/videojuegos.jpg\" alt=\"videojuegos\"></a>\n" +
"                </div>\n" +
"                <div class =\"col-1-5 hide\" ></div>\n" +
"            </div>\n" +

 "       </div>\n" +
 " </asp:Content> \n" +
  "<asp:Content ID=\"Content4\" ContentPlaceHolderID=\"maincontentID\" runat=\"server\"> \n" +
" <div id=\"maincontent\" style=\"background-image: url(../../../../images/pattern3.png); width: 100%;\">\n" +
 "           <div class=\"row\">\n" +
 "                <div class =\"col-1-5 hide\" ></div>\n" +
  "  <div class = \"col-3 col-mm-4 col-m-12\"><img src=\"images/imgpral.jpg\" alt=\"imgpral\" style=\"width: 100%;\" /></div>                                     \n" +
         "  <div class = \"col-6 col-mm-8 col-m-12\">                                     \n" +
          " <h2> " + titulo + "    </h2>                                 \n" +
 contenido +
 "     <div class =\"col-1-5 hide\" ></div>\n" +

        " </div> \n" +
       " </div> \n" +
       "<div class=\"row\">\n" +
"<div class=\"col-1-5 hide\">&nbsp;</div>\n" +

"<div class=\"col-9 col-m-12 col-mm-12\">\n" +
"<% if(Session[\"sid\"] != null){ %>\n" +
"<form name=\"commentForm\" method=\"post\" action=\"http://localhost:8085/AniTechVgPHP/commentary/insertComment.php\">\n" +
"	<label for =\"com\">Dejanos tu comentario:</label>\n" +
"	<br>\n" +
"	<textarea name=\"com\" rows=\"5\" cols=\"45\" maxlength=\"250\" style=\"width: 100%;\" required></textarea>\n" +
"	<input type=\"hidden\" name = \"usid\" id=\"usid\" value=\"<%= Session[\"usid\"]%>\"></input>\n" +
"	<input type=\"hidden\" name = \"artid\" id=\"artid\" value=\" " + inserted + "\"></input>\n" +
"	<input type=\"submit\"></input>\n" +
"</form>\n" +
"<% \n" +
"} else {\n" +
"%>\n" +
"<p>Debes <a href=\"http://localhost:8080/AniTechVg/index.jsp\">iniciar sesión</a> para poder comentar.</p>\n" +
"<% \n" +
"} \n" +
"%>\n" +
"<div class=\"col-1-5 hide\">&nbsp;</div>\n" +
"</div>\n" +
"</div> \n" +
"<div class=\"row\">\n" +
"<div class=\"col-1-5 hide\">&nbsp;</div>\n" +
"<div class=\"col-9 col-m-9 col-m\">\n" +
"<% \n" +
"	var instance = new AniTechVg.DataLayer();\n" +
"	var list = instance.getComments(" + inserted + ");\n" +
"	if(list != null) {\n" +
"		foreach (AniTechVg.Comment x in list)\n" +
"		{\n" +
"			%>\n" +
"			<p>Fecha del comentario:<%= x.fechacoment%></p>		\n" +
"			<p>Comentario:</p>				\n" +
"			<p><%= x.comentario%></p>\n" +
"			<%\n" +
    "	}\n" +
    "}\n" +

 "%>\n" +

"</div>\n" +
"<div class=\"col-1-5 hide\">&nbsp;</div>\n" +
"</div> \n" +
   " </asp:Content> \n" +
     "<asp:Content ID=\"Content5\" ContentPlaceHolderID=\"footer\" runat=\"server\"> \n" +
"       <footer class=\"footer-distributed\">\n" +

 "           <div class=\"footer-left\">\n" +

  "              <img class =\"footer-img\" src=\"../../../../images/logofinal2.png\"/>\n" +

   "             <p class=\"footer-links\">\n" +
    "                <a href=\"#\">Home</a>\n" +
     "               ·\n" +
      "              <a href=\"#\">Blog</a>     \n" +
       "             ·\n" +
        "            <a href=\"#\">About</a>\n" +
         "           ·\n" +
          "          <a href=\"#\">Faq</a>\n" +
           "         ·\n" +
            "        <a href=\"#\">Contact</a>\n" +
             "   </p>\n" +

             "   <p class=\"footer-company-name\">AniTechVg &copy; 2015</p>\n" +
            "</div>\n" +

            "<div class=\"footer-center\">\n" +

             "   <div>\n" +
              "      <i class=\"fa fa-map-marker\"></i>\n" +
              "      <p><span>Av. Madero 255</span> Morelia, Mexico</p>\n" +
              "  </div>\n" +

               " <div>\n" +
                "    <i class=\"fa fa-phone\"></i>\n" +
                 "   <p>+1 555 123456</p>\n" +
                "</div>\n" +

                "<div>\n" +
                 "   <i class=\"fa fa-envelope\"></i>\n" +
                  "  <p><a href=\"mailto:support@anitech.vg\">support@anitech.vg</a></p>\n" +
                "</div>\n" +

            "</div>\n" +

            "<div class=\"footer-right\">\n" +

             "   <p class=\"footer-company-about\">\n" +
              "      <img class =\"footer-img\" src=\"images/contactanos.jpg\" alt=\"contactanos\"/>\n" +
              "  </p>\n" +

               " <div class=\"footer-icons\">\n" +

                "    <a href=\"http://www.facebook.com/AniTechVG\"><img src=\"../../../../images/facebook64x64.png\"/></a>\n" +
                 "   <a href=\"http://www.twitter.com/AniTechVG\"><img src=\"../../../../images/twitter64x64.png\"/></a>\n" +
                  "  <a href=\"http://www.g.in/AniTechVG\"><img src=\"../../../../images/google64x64.png\"/></a>\n" +


                "</div>\n" +

            "</div>\n" +

        "</footer>\n" +
        " </asp:Content> \n";

                _testData.WriteLine(contenido); // Write the file.
            }

            using (StreamWriter _testData = new StreamWriter(Server.MapPath("~/"+ loc + "/Default.aspx.cs"), true))
            {
                contenido = "using System;\n" +
"using System.Collections.Generic; \n" +
"using System.Linq; \n" +
"using System.Web; \n" +
"using System.Web.UI; \n" +
"using System.IO; \n" +
"using System.Drawing; \n" +
"using System.Drawing.Imaging; \n" +
"    namespace AniTechVg \n" +
"{\n" +
"    public partial class WebForm1 : System.Web.UI.Page \n" +
"    {\n" +        
      "  protected void Page_Load(object sender, EventArgs e) \n" +
      "  { \n" +
      "  } \n" +
   " } \n" +
"} \n" ;

                _testData.WriteLine(contenido); // Write the file.
            }
        }
    }
}