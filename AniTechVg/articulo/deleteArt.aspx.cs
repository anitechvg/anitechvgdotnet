﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AniTechVg.articulo
{
    public partial class deleteArt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var nombreart = Request.Form["id"];
                var instance = new DataLayer();
                instance.deleteArt(Convert.ToInt32(nombreart));
            }
        }
    }
}