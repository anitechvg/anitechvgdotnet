﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AniTechVg.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AniTechVg.WebForm1" %>

<asp:Content ID="Content6" ContentPlaceHolderID="maincontentID" runat="server">
    <div id ="maincontent" style="background-image: url(images/pattern3.png); width: 100%;">
        <div class="row">
            <div class = "col-1 col-m-1 hide"></div>
                <%                    
                    String tipoq = "", query ="";
                    int offset = 0, celda = 0 ;
                    if (Request.QueryString["tipo"] != null)
                    {
                        tipoq = "where tipo = "+ Request.QueryString["tipo"];
                    }
                    else
                    {
                        tipoq = "";
                    }
                    
                    if (Request.QueryString["cat"] != null) {
                        tipoq = "where categoria = " + Request.QueryString["cat"];
                    }                    
                    
                    if (Request.QueryString["page"] != null)
                    {
                        offset = (Convert.ToInt32(Request.QueryString["page"]) - 1) * 9;
                        query = "select * from (select top 9 * from articulos) as art " + tipoq + " order by id desc offset " + offset + " rows";
                    }
                    else
                    {
                        query = "select * from (select top 9 * from articulos) as art " + tipoq + " order by id desc offset " + offset + " rows";
                    }
                        
                    var someValueFromGet = Request.QueryString["YourGetPara"];
                    
                    string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ConnectionS1"].ConnectionString;
                    System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connectionString);
                    conn.Open();
                    using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(query, conn))
                    {
                        try
                        {
                            System.Data.SqlClient.SqlDataReader myReader = cmd.ExecuteReader();
                        while (myReader.Read())
                        {
                            if (celda < 2)
                            {
                                %>                            
                            <div class = "col-3 col-m-3 col-mm-12 newslistelement">
                                <div class="box">
                            <img class = "imgpral" src="<%=myReader["rutaimgpral"].ToString() %>"  style="width: 100%;"/>
                            <div class="overbox">
                                <div class="title"><%=myReader["titulo"].ToString() %></div>                                
                            </div>
                        </div> 
                                <div class = "infoart">
                                    <div class = "row">
                                    <div class = "col-8 col-mm-8 col-m-12">	
                    	                    <img class = "artimg" src="<%=myReader["rutaimgpral"].ToString() %>" style="width: 100%;"/>		
                                    </div>
                                    <div class = "col-4 col-mm-4 col-m-12">
                                          <h3><%=myReader["titulo"].ToString() %></h3>                                     
                                                <p><%=myReader["descripcion"].ToString() %></p>

                                                <a class = "smshare" href="https://facebook.com/"><img src="images/facebook64x64.png" style="width: 32px; height: 32px;"/></a>
                                                <a class = "smshare" href="https://google.com/"><img src="images/google64x64.png" style="width: 32px; height: 32px;"/></a>
                                                <a class = "smshare" href="https://twitter.com/"><img src="images/twitter64x64.png" style="width: 32px; height: 32px;"/></a>                            
                            
                                                    <a href="http://localhost:11288/<%=myReader["rutaart"].ToString() %>/Default.aspx">Leer mas</a>
                                    </div>                 
                                </div>    
                        </div>
                            </div>
                            <div class = "col-05 col-m-05 hide"></div>
                <%          celda++;
                            }
                            else
                            {
                              %>
                               <div class = "col-3 col-m-3 col-mm-12 newslistelement">
                                <div class="box">
                            <img class = "imgpral" src="<%=myReader["rutaimgpral"].ToString() %>"  style="width: 100%;"/>
                            <div class="overbox">
                                <div class="title"><%=myReader["titulo"].ToString() %></div>                                
                            </div>
                        </div> 
                            <div class = "infoart">
                                    <div class = "row">
                                    <div class = "col-8 col-mm-8 col-m-12">	
                    	                    <img class = "artimg" src="<%=myReader["rutaimgpral"].ToString() %>" style="width: 100%;"/>		
                                    </div>
                                    <div class = "col-4 col-mm-4 col-m-12">
                                          <h3><%=myReader["titulo"].ToString() %></h3>                                     
                                                <p><%=myReader["descripcion"].ToString() %></p>

                                                <a class = "smshare" href="https://facebook.com/"><img src="images/facebook64x64.png" style="width: 32px; height: 32px;"/></a>
                                                <a class = "smshare" href="https://google.com/"><img src="images/google64x64.png" style="width: 32px; height: 32px;"/></a>
                                                <a class = "smshare" href="https://twitter.com/"><img src="images/twitter64x64.png" style="width: 32px; height: 32px;"/></a>                            
                            
                                                    <a href="http://localhost:11288/<%=myReader["rutaart"].ToString() %>/Default.aspx">Leer mas</a>
                                    </div>                 
                                </div>    
                        </div>
                            </div>
                            <div class = "col-1 col-m-1 hide"></div>
                         </div>
                            <div class="row">
                                <div class = "col-1 col-m-1 hide"></div>
                             <%
                                celda = 0;
                            }
                        }
                        } catch (Exception e)
                        {
                            Response.Write(e.ToString());
                        }
                        
                        
                    }
                    
                     %>
            
        </div>
    </div>

    <script>
        $(document).ready(function () {

            $("div.newslistelement").bind("click", function () {

                $("div.infoart", this).clone().appendTo("div#wrapper-cnt");

                $("div.infoart").show();
                $("div#wrapper").css({ 'display': 'table' });
                $("div#wrapper").show();
                $("div.box").hide();
                
            });

            $("div#wrapper").click(function () {
                $("div#wrapper-cnt", this).html("");
                $("div.infoart").hide();
                $("div#wrapper").hide();
                $("div.box").show();
            });

        });
    </script>
</asp:Content>
